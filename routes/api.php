<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\API\DeskController;

use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\ProjectController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/register', [RegisterController::class, 'register']);
Route::post('/login', [RegisterController::class, 'login']);

Route::middleware('auth:sanctum')->group( function () {
    Route::resource('/projects', ProjectController::class);
    Route::get('/projects', [ProjectController::class, 'index'])->name('index');
    Route::get('/projects/{project}', [ProjectController::class, 'show'])->name('show');
    Route::post('/projects', [ProjectController::class, 'store'])->name('store');
    Route::put('/projects/{project}', [ProjectController::class, 'update'])->name('update');
    Route::patch('/projects/{project}', [ProjectController::class, 'update']);
    Route::delete('/projects/{project}', [ProjectController::class, 'destroy'])->name('destroy');
    
});

