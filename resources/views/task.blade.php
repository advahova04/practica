<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Tasks List</title>
    <style>
        table {
    width: 100%;
    border-collapse: collapse;
}

table th, table td {
    padding: 8px;
    text-align: left;
    border: 1px solid #ddd;
}

table th {
    background-color: #f2f2f2;
}

table tbody tr:nth-child(even) {
    background-color: #f9f9f9;
}

        </style>
</head>
<body>
    
    <table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Attachments</th>
            <th>Due Date</th>
            <th>Project</th>
            <th>Priority</th>
            <th>Status</th>
            <th>Assigned To</th>
            <th>Submitter</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tasks as $task)
            <tr>
                <td>{{ $task->id }}</td>
                <td>{{ $task->name }}</td>
                <td>{{ $task->description }}</td>
                <td>{{ $task->attachments }}</td>
                <td>{{ $task->due_date }}</td>
                <td>{{ $task->project->name }}</td>
                <td>{{ $task->priority }}</td>
                <td>{{ $task->status }}</td>
                <td>{{ $task->assigned_to }}</td>
                <td>{{ $task->submiter }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>
