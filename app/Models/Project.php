<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $fillable = [
        'name', 'description', 'category', 'start_date', 'end_date', 'client_id', 'status'
    ];

    public function tasks()
    {
      return $this->hasMany(Task::class);
    }

    public function canBeDeleted()
    {
      return $this->tasks()->count() === 0;
    }
}
