<?php

namespace App\Models;
use App\Models\Project;
use App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'tasks';
    protected $guarded = [];
    

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function assigned_to()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function submiter()
    {
        return $this->belongsTo(User::class, 'submiter');
    }

}
