<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Resources\Project;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ProjectPostRequest;
use Illuminate\Http\Request;
use App\Models\Project as ProjectModel;

class ProjectController extends DeskController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project = ProjectModel::get();
    
        return $this->sendResponse(Project::collection($project), 'Project retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectPostRequest $request)
    {
        $input = $request->post();
   
        $project = ProjectModel::create($input);
   
        return $this->sendResponse(new Project($project), 'Project created successfully.', '201');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = ProjectModel::find($id);
  
        if (is_null($project)) {
            return $this->sendError('Project not found.');
        }
   
        return $this->sendResponse(new Project($project), 'Project retrieved successfully.', '201');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectPostRequest $request, ProjectModel $project)
    {
        $input = $request->post();

        $project->name = $input['name'];
        $project->description = $input['description'];
        $project->category = $input['category'];
        $project->start_date = $input['start_date'];
        $project->end_date = $input['end_date'];
        $project->client_id = $input['client_id'];
        $project->status = $input['status'];
        $project->save();
   
        return $this->sendResponse(new Project($project), 'Project updated successfully.', '202');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectModel $project)
    {
        if (!$project) {
            return $this->sendError('Project not found.');
        }
        
        if (!$project->canBeDeleted()) {
            return $this->sendError('Project can not be deleted. It has related tasks.');
        }
    
        $project->delete();
       
        return $this->sendResponse([], 'Project deleted successfully.');
    }
    
}
