<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class AuthenticateAPI extends Middleware
{
   public function authenticate($request, array $quards){
   $token = $request->query('api_token');
   if(empty($token)){
    $token = $request->input('api_token');
   }
   if(empty($token)){
    $token = $request->bearerToken('api_token');
   }
   if($token === config('apitokens'[0])) return;
   $this -> unauthenticated($request, $quards);
   }
}
