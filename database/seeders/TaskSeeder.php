<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $tasks = [
            ['title' => 'Task 1', 'description' => 'Description 1'],
            ['title' => 'Task 2', 'description' => 'Description 2'],
            ['title' => 'Task 3', 'description' => 'Description 3'],
        ];
    
        foreach ($tasks as $task) {
            Task::create($task);
        }
    }
    
}
