<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->text('description');
            $table->string('attachments', 30)->nullable();
            $table->date('due_date');
            $table->unsignedBigInteger('project_id');
            $table->enum('priority', ['low', 'medium', 'high', 'urgent'])->default('low');
            $table->enum('status', ['pending', 'in_progress', 'completed', 'cancelled'])->default('pending');
            $table->unsignedBigInteger('assigned_to');
            $table->unsignedBigInteger('submiter');
            $table->foreign('project_id')->references('id')->on('projects'); // Устанавливаем внешний ключ для связи с проектами
            $table->foreign('assigned_to')->references('id')->on('users'); // Устанавливаем внешний ключ для связи с проектами
            $table->foreign('submiter')->references('id')->on('users'); // Устанавливаем внешний ключ для связи с проектами
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
